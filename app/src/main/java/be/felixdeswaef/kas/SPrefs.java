package be.felixdeswaef.kas;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.Preference;
import android.preference.PreferenceManager;

public class SPrefs {
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    public SPrefs (Context context){
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        editor = prefs.edit();

    }
    public void setNightmode(boolean state){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("DARK_LAY",state);
    }
    public boolean readNightmode(){
        return prefs.getBoolean("DARK_LAY",false);
    }


}
