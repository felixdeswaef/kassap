package be.felixdeswaef.kas;

import com.google.gson.annotations.SerializedName;
//{"id":1,"name":"Heaven Morissette","price":10.57,"placeid":22,"created_at":"2019-05-18 15:16:05","updated_at":"2019-05-18 15:16:05"},
//{"id":1,"name":"Mr. Edwardo Johnston","type":"Monday","phone":"1-682-716-3377","created_at":"2019-05-05 07:58:15","updated_at":"2019-05-05 07:58:15"}
public class item {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("price")
    private float price;
    @SerializedName("placeid")
    private Integer placid;

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public float getPrice() {
        return this.price;
    }

    public Integer getPlace() {
        return this.placid;
    }

    public carddata cardify(){
        return new carddata(this.name,this.price + "","","", this.id,"ITE");
    }
}
