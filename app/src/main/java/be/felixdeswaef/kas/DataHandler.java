package be.felixdeswaef.kas;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import be.felixdeswaef.kas.database.DBhelper;
import be.felixdeswaef.kas.database.model.LOC_ELEM;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.ContentValues.TAG;

public class DataHandler {
    private static FDS_BE_S serviceIF;
    MainActivity main;
    public boolean scheck = false;
    //raw
    private List<fplace> RAWPlaces;
    private List<bill> RAWBill;
    private List<billit> RAWBillit;
    private List<item> RAWItems;
    //smart
    private List<Splace> Splaces;
    private List<Sbill> Sbills;
    private List<Sbillit> Sbillits;
    private List<Sitem> Sitems;

    private boolean CPLA = false;
    private boolean CBIL = false;
    private boolean CBIT = false;
    private boolean CITE = false;


    public DBhelper database;
    public DataHandler(MainActivity m){
        main = m;

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://felixdeswaef.be/api/")
                //.baseUrl("http://192.168.10.10/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        serviceIF = retrofit.create(FDS_BE_S.class);
        database = new DBhelper( main.getApplicationContext());
        database.onUpgrade(database.getWritableDatabase(),0,0);
        database.seed();
        Log.d(TAG,database.getAllElem().get(0).getElem() + database.getElemCount());

    }
    void failure() {
    }

    boolean check() {
        return CPLA && CBIL && CBIT && CITE;
    }

    void Smart() {
        if (check()){
            SmartPlaces();

            SmartItems();
            SmartBills();
            SmartBillItems();
            scheck = true;
            main.DHUpdate();
        }
    }

    FDS_BE_S getService() {
        return serviceIF;
    }

    void UpdateAll() {
        this.UpdateLines();
        this.UpdatePlaces();
        this.UpdateItems();
        this.UpdateBills();




    }
    carddata[] dbelcard() {
        int len = database.getElemCount();
        carddata[] cardstack = new carddata[len];
        List<LOC_ELEM> list = database.getAllElem();
        for (int i = 0; i < len; i++) {
            carddata t = new carddata(list.get(i).getElem(),"DB EL","deze card komt uit de db",list.get(i).getId()+"",list.get(i).getId());

            cardstack[i]=(t);

        }
        return cardstack;
    }
    carddata[] Foodcards(Splace place) {
        carddata[] cardstack = new carddata[Sbills.size()];
        for (int i = 0; i < place.items.size(); i++) {
            cardstack[i]=(place.items.get(i).item.cardify());

        }
        return cardstack;
    }

    carddata[] Billcards() {
        carddata[] cardstack = new carddata[Sbills.size()];
        //if (Sbills == null)return;
        for (int i = 0; i < Sbills.size(); i++) {
            cardstack[i]=(Sbills.get(i).bill.cardify());

        }
        return cardstack;
    }

    carddata[] Placecards() {
        carddata[] cardstack = new carddata[Sbills.size()];
        for (int i = 0; i < Sbills.size(); i++) {
            cardstack[i]=(Sbills.get(i).bill.cardify());

        }
        return cardstack;
    }

    Splace FindPlace(Integer idv) {

        for (int i = 0; i < Splaces.size(); i++) {
            if (Splaces.get(i).id == idv) return Splaces.get(i);

        }
        return null;
    }

    void SmartPlaces() {
        List<Splace> TSPlaces = new ArrayList<>();
        for (int i = 0; i < RAWPlaces.size(); i++) {

            TSPlaces.add(new Splace(RAWPlaces.get(i), this));
        }
        Splaces = TSPlaces;
    }

    void UpdatePlaces() {
        Call<List<fplace>> call = serviceIF.places();
        call.enqueue(new Callback<List<fplace>>() {


            @Override
            public void onResponse(Call<List<fplace>> call, Response<List<fplace>> response) {
                List<fplace> places = response.body();
                //recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
                RAWPlaces = places;

                CPLA = true;
                Smart();
            }

            @Override
            public void onFailure(Call<List<fplace>> call, Throwable throwable) {
                failure();
            }


        });

    }

    void SmartItems() {
        List<Sitem> TSItem =  new ArrayList<>();
        for (int i = 0; i < RAWItems.size(); i++) {

            TSItem.add(new Sitem(RAWItems.get(i), this));
        }
        Sitems = TSItem;
    }

    void UpdateItems() {
        Call<List<item>> call = serviceIF.items();
        call.enqueue(new Callback<List<item>>() {


            @Override
            public void onResponse(Call<List<item>> call, Response<List<item>> response) {
                List<item> items = response.body();
                //recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
                RAWItems = items;

                CITE = true;
                Smart();
            }

            @Override
            public void onFailure(Call<List<item>> call, Throwable throwable) {
                failure();
            }


        });

    }

    Sbill FindBill(Integer idv) {
        for (int i = 0; i < Sbills.size(); i++) {
            if (Sbills.get(i).id == idv) return Sbills.get(i);

        }
        return null;
    }

    void SmartBills() {
        List<Sbill> TSBills =  new ArrayList<>();
        for (int i = 0; i < RAWBill.size(); i++) {

            TSBills.add(new Sbill(RAWBill.get(i), this));
        }
        Sbills = TSBills;
    }

    void UpdateBills() {
        Call<List<bill>> call = serviceIF.bills();
        call.enqueue(new Callback<List<bill>>() {


            @Override
            public void onResponse(Call<List<bill>> call, Response<List<bill>> response) {
                List<bill> Bills = response.body();
                //recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
                RAWBill = Bills;

                CBIL = true;
                Smart();
            }

            @Override
            public void onFailure(Call<List<bill>> call, Throwable throwable) {
                failure();
            }


        });



    }

    void SmartBillItems() {
        List<Sbillit> TSBillit =  new ArrayList<>();
        for (int i = 0; i < RAWBillit.size(); i++) {

            TSBillit.add(new Sbillit(RAWBillit.get(i), this));
        }
        Sbillits = TSBillit;

    }

    void UpdateLines() {
        Call<List<billit>> call = serviceIF.billitem();
        call.enqueue(new Callback<List<billit>>() {


            @Override
            public void onResponse(Call<List<billit>> call, Response<List<billit>> response) {
                List<billit> billitems = response.body();
                //recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
                RAWBillit = billitems;

                CBIT = true;
                Smart();
            }

            @Override
            public void onFailure(Call<List<billit>> call, Throwable throwable) {
                failure();
            }


        });

    }



}
