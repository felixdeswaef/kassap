package be.felixdeswaef.kas.database.model;

public class LOC_ELEM {

        public static final String TABLE_NAME = "LOC_ELEM";

        public static final String COLUMN_ID = "id";
        public static final String COLUMN_NOTE = "content";
        public static final String COLUMN_TIMESTAMP = "timestamp";

        private int id;
        private String elem;
        private String timestamp;


        // Create table SQL query
        public static final String CREATE_TABLE =
                "CREATE TABLE " + TABLE_NAME + "("
                        + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + COLUMN_NOTE + " TEXT,"
                        + COLUMN_TIMESTAMP + " DATETIME DEFAULT CURRENT_TIMESTAMP"
                        + ")";

        public LOC_ELEM() {
        }

        public LOC_ELEM(int id, String elem, String timestamp) {
            this.id = id;
            this.elem = elem;
            this.timestamp = timestamp;
        }

        public int getId() {
            return id;
        }

        public String getElem() {
            return elem;
        }

        public void setElem(String elem) {
            this.elem = elem;
        }

        public String getTimestamp() {
            return timestamp;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }
    }

