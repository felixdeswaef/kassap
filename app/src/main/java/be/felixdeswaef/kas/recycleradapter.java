package be.felixdeswaef.kas;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class recycleradapter extends RecyclerView.Adapter<recycleradapter.MyViewHolder> {


    private carddata[] mDataset;
    static private ListItemClickListener mlistner;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        public ConstraintLayout c;
        public MyViewHolder(ConstraintLayout v) {
            super(v);
            c = v;
            itemView.setOnClickListener(this);


        }
        public void onClick(View view) {
            int clickedPosition = getAdapterPosition();
            mlistner.onListItemClick(clickedPosition);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public recycleradapter(carddata[] myDataset,ListItemClickListener listener) {
        mDataset = myDataset;
        mlistner = listener;
    }


    // Create new views (invoked by the layout manager)
    @Override
    public recycleradapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        ConstraintLayout v = (ConstraintLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.place_card, parent, false);


        MyViewHolder vh = new MyViewHolder(v);

    return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        ConstraintLayout cl =  ((ConstraintLayout) ((CardView)holder.c.getChildAt(0)).getChildAt(0));
        TextView title =(TextView) cl.getViewById(R.id.CARDTITLE);
        TextView left =(TextView) cl.getViewById(R.id.LEFTTOPTEXT);
        TextView tline =(TextView) cl.getViewById(R.id.TLINE1);
        TextView bline =(TextView) cl.getViewById(R.id.BLINE);

        title.setText(mDataset[position].Title);
        left.setText(mDataset[position].Left);
        tline.setText(mDataset[position].TLine);
        bline.setText(mDataset[position].BLine);    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.length;
    }
}


