package be.felixdeswaef.kas;

import com.google.gson.annotations.SerializedName;

//{"id":1,"name":"Mr. Edwardo Johnston","type":"Monday","phone":"1-682-716-3377","created_at":"2019-05-05 07:58:15","updated_at":"2019-05-05 07:58:15"}
public class fplace {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("Type")
    private String type;
    @SerializedName("phone")
    private String phone;

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getType() {
        return this.type;
    }

    public String getPhone() {
        return this.phone;
    }
    public carddata cardify() {
        return new carddata(this.getName(),this.type,this.phone,"",this.id);
    }
}


