package be.felixdeswaef.kas;

import android.content.SharedPreferences;
import android.preference.Preference;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

public class settings extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    SPrefs prefinterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefinterface = new SPrefs(getApplicationContext());
        prefinterface.prefs.registerOnSharedPreferenceChangeListener(this);

        if (prefinterface.readNightmode()) setTheme(R.style.Darkmode);
        setContentView(R.layout.activity_settings);


        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.setbox, new SET_FRAG())
                .commit();


    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals("DARK_LAY")) {
            recreate();


        }
    }

    void changeusername(String un) {

    }

}
