package be.felixdeswaef.kas;

import java.util.ArrayList;
import java.util.List;

public class Splace {
    public Integer id;
    public fplace place;
    public List<Sitem> items = new ArrayList<>();
    public Splace(fplace pl,DataHandler DH)
    {
        this.place = pl;
        this.Smartify(DH);
    }
    void Smartify(DataHandler DH){
        id = this.place.getId();
    }
    void subscribe(Sitem it){
        items.add(it);
    }

}
