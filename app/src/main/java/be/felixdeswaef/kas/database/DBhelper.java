package be.felixdeswaef.kas.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.nfc.Tag;
import android.provider.ContactsContract;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import be.felixdeswaef.kas.database.model.LOC_ELEM;

import static android.content.ContentValues.TAG;


public class DBhelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "KAS_db";


    public DBhelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        // create notes table
        db.execSQL(LOC_ELEM.CREATE_TABLE);


    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + LOC_ELEM.TABLE_NAME);

        // Create tables again
        onCreate(db);


    }

    public void seed( ) {
        for(int i = 0 ; i<10 ;i++) {

            this.insertElem("test ELEMENT " + i);
        }

    }


    ///functional prep


///pre


    public long insertElem(String note) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        values.put(LOC_ELEM.COLUMN_NOTE, note);

        // insert row
        long id = db.insert(LOC_ELEM.TABLE_NAME, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }

    public LOC_ELEM getElem(long id) {


        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(LOC_ELEM.TABLE_NAME,
                new String[]{LOC_ELEM.COLUMN_ID, LOC_ELEM.COLUMN_NOTE, LOC_ELEM.COLUMN_TIMESTAMP},
                LOC_ELEM.COLUMN_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        // prepare loc object
        LOC_ELEM note = new LOC_ELEM(
                cursor.getInt(cursor.getColumnIndex(LOC_ELEM.COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(LOC_ELEM.COLUMN_NOTE)),
                cursor.getString(cursor.getColumnIndex(LOC_ELEM.COLUMN_TIMESTAMP)));

        // close the db connection
        cursor.close();

        return note;
    }

    public List<LOC_ELEM> getAllElem() {

        List<LOC_ELEM> notes = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + LOC_ELEM.TABLE_NAME + " ORDER BY " +
                LOC_ELEM.COLUMN_TIMESTAMP + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                LOC_ELEM Lelem = new LOC_ELEM();
                Lelem.setId(cursor.getInt(cursor.getColumnIndex(LOC_ELEM.COLUMN_ID)));
                Lelem.setElem(cursor.getString(cursor.getColumnIndex(LOC_ELEM.COLUMN_NOTE)));
                Lelem.setTimestamp(cursor.getString(cursor.getColumnIndex(LOC_ELEM.COLUMN_TIMESTAMP)));

                notes.add(Lelem);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return notes;
    }

    public int getElemCount() {
        String countQuery = "SELECT  * FROM " + LOC_ELEM.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();


        // return count
        return count;
    }
}
