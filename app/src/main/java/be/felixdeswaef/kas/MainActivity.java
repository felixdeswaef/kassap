package be.felixdeswaef.kas;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SharedPreferences.OnSharedPreferenceChangeListener, View.OnClickListener, ListItemClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    //fooddata
    private View current;
    private Boolean RequestedUpdate = true;
    private Sbill selected;

    private static DataHandler Backend;
    private Boolean ordering = true;
    private static int step = 0;//0 Bill , 1 Place , 2 item , 3 display bill , 4 display personal total
    LinearLayoutManager layoutManager;
    public String SYSKEY;
    private SPrefs prefinterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefinterface = new SPrefs(getApplicationContext());
        prefinterface.prefs.registerOnSharedPreferenceChangeListener(this);

        if (prefinterface.readNightmode()) setTheme(R.style.Darkmode);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        initfoodstuff();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.root);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.bringToFront();
        current = findViewById(R.id.maincon);


        if (Backend == null) {
            Backend = new DataHandler(this);
            Backend.UpdateAll();
        }
        else
        {
            foodrederer();
        }



    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        foodrederer();
    }

        void initfoodstuff() {


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabbut);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (step == 1) {
                    Backend.database.insertElem("Fab added this");
                    foodrederer();
                } else {
                    Snackbar.make(view, "adding elements is not yet possible on mobile", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }
        });


    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals("DARK_LAY")) {
            recreate();

        }
    }

    void initgeneral() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.root);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.bringToFront();

    }

    void startAnim() {
        findViewById(R.id.loading).setAlpha(1);
    }

    void stopAnim() {
        findViewById(R.id.loading).setAlpha(0);
        // or avi.smoothToHide();
    }

    public void DHUpdate() {

        if (RequestedUpdate) {
            RequestedUpdate = false;
            foodrederer();
            stopAnim();

        }

    }

    public void prev(View vieuw) {
        if (step == 1) step = 0;
        else if (step == 2) step = 1;
        else if (step == 3) step = 2;
        foodrederer();
    }

    public void next(View vieuw) {
        if (step == 1) step = 2;
        else if (step == 2) step = 3;
        else if (step == 0) step = 1;
        foodrederer();
    }

    public void foodrederer() {
        String ptag;
        if (!Backend.scheck) {
            RequestedUpdate = true;
            return;
        }

        switch (this.step) {
            case 1://select bill
                selectDBELMENU();
                ptag="new Bill (cacheDB)";
                break;
            case 2://select food
                foodmenu();
                ptag="Select food (WIP)";
                break;
//case default
            default:
                selectbillmenu();
                //selectDBELMENU();
                ptag="Select Bill";

        }
        TextView tv =(TextView) findViewById(R.id.PAGTIT);
        if(tv!=null) tv.setText(ptag);
        stopAnim();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.root);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            opensettings();
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_dashboard) {
            opendashbord();
        } else if (id == R.id.nav_food) {
            foodpage();

        } else if (id == R.id.nav_login) {
            loginpage();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.root);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void additem() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.root);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void selectbillmenu() {
        loadcards((carddata[]) Backend.Billcards());

    }
    public void selectDBELMENU() {
        loadcards((carddata[]) Backend.dbelcard());

    }
    public void newbillmenu() {
        loadcards((carddata[]) Backend.Foodcards(Backend.FindPlace(this.selected.bill.getplace())));

    }

    public void closekeyb() {
        //TODO close kb!
        //Keyboard.
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(findViewById(R.id.root).getWindowToken(), 0);
    }

    public void loginpage() {
        DrawerLayout con = (DrawerLayout) findViewById(R.id.root);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.app_bar_login, null);

        closekeyb();
        con.removeView(current);
        con.addView(layout);
        initgeneral();
        current = layout;
        final EditText pwd = findViewById(R.id.pwd);
        final EditText usr = findViewById(R.id.usr);
        pwd.setImeActionLabel("GO", KeyEvent.KEYCODE_ENTER);
        pwd.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    tokr(usr.getText().toString(), pwd.getText().toString());
                    return true;
                }
                return false;
            }

        });
    }

    void tokr(String usr, String pwd) {

        tokenr req = new tokenr().make(usr, pwd);
        Call<tokenret> call = Backend.getService().tokener(req);
        call.enqueue(new Callback<tokenret>() {


            @Override
            public void onResponse(Call<tokenret> call, Response<tokenret> response) {
                //Log.d(TAG, response.body() + " " + response.body().getmessage());
                if (response.body() == null) return;
                if (response.body().getmessage().equals("ACCEPTED")) {
                    SYSKEY = response.body().getkey();
                    foodpage();
                } else {
                    Snackbar.make(findViewById(R.id.pwd), "Credentials invalid", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }

            @Override
            public void onFailure(Call<tokenret> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }


        });

    }

    @Override
    public void onClick(View view) {
        Log.d(TAG, view.toString());
    }

    public void loadcards(carddata[] ds) {
        RecyclerView rec = (RecyclerView) findViewById(R.id.recycler_box);
        //RecyclerView rec = (RecyclerView) findViewById(R.id.recycler_box);
        //rec.addView();
        //rec.child
        layoutManager = new LinearLayoutManager(this);
        rec.setLayoutManager(layoutManager);

        recycleradapter rca = new recycleradapter(ds, this);

        rec.setAdapter(rca);


    }

    @Override
    public void onListItemClick(int count) {
        Log.d(TAG,count+" : "+this.step);
        Snackbar.make( findViewById(R.id.root), "clicked element " + count+"on page "+this.step, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    public void foodmenu() {
        if (this.selected == null)
        {
            carddata[] card = new carddata[1];
            card[0] = new carddata("error","","there is probably no bill selected","",1);
            loadcards(card);
            return;
        }
        loadcards((carddata[]) Backend.Foodcards(Backend.FindPlace(this.selected.bill.getplace())));
    }

    public void opensettings() {
        Intent setingsintent = new Intent(this, settings.class);
        startActivity(setingsintent);
    }
    public void opendashbord() {
        Intent dashintent = new Intent(this, Main2Activity.class);
        startActivity(dashintent);
    }

    public void foodpage() {
        DrawerLayout con = (DrawerLayout) findViewById(R.id.root);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.app_bar_main, null);
        closekeyb();
        con.removeView(current);
        con.addView(layout);
        current = layout;
        initgeneral();
        initfoodstuff();
        foodrederer();

    }


}



