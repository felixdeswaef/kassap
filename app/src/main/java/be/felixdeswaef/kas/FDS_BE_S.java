package be.felixdeswaef.kas;

import java.util.List;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface FDS_BE_S {
    @POST("tokenr")
    Call<tokenret> tokener(@Body tokenr body);


    @GET("fplace")// + "?api_token="+SYSKEY)
    Call<List<fplace>> places();
    @GET("fplace/{id}")
    Call<fplace> place(@Path("id") int placeid);


    @GET("fitem")
    Call<List<item>> items();
    @GET("fitem/{id}")
    Call<item> item(@Path("id") int itemid);


    @GET("fitemat/{id}")
    Call<List<item>> itemsat(@Path("id") int placeid);

    @GET("fbill")
    Call<List<bill>> bills();
    @GET("fbill/{id}")
    Call<bill> bill(@Path("id") int billid);

    @GET("fbitem")
    Call<List<billit>> billitem();
    @GET("fbitem/{idb}/{id}")
    Call<billit> billitemsp(@Path("idb") int billid ,@Path("id") int billitemid);
}
