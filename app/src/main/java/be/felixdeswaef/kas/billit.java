package be.felixdeswaef.kas;

import com.google.gson.annotations.SerializedName;



/*

'subselect' =>$faker->dayOfWeek,
        'billid'=> factory(App\foodbills::class)->create()->id,
        'itemid'=>factory(App\fooditems::class)->create()->id,
        'username'=>$faker->name,
        'count'=>$faker->numberBetween(1,10),
        'userid'=> factory(App\User::class)->create()->id,

[{"id":1,"subselect":"Friday","username":"Prof. Dina Quitzon","count":4,"billid":1,"itemid":1,"userid":21,"created_at":"2019-05-18 15:16:05","updated_at":"2019-05-18 15:16:05"},



 */
public class billit {
    @SerializedName("id")
    private int id;
    @SerializedName("billid")
    private int billid;
    @SerializedName("count")
    private int count;
    @SerializedName("itemid")
    private int itemid;
    @SerializedName("userid")
    private int userid;
    @SerializedName("username")
    private String username;
    @SerializedName("printname")
    private String printname;
    @SerializedName("subselect")
    private String subsel;


    public int getId() {
        return this.id;
    }
    public int getBillId() {
        return this.billid;
    }
    public int getCount() {
        return this.count;
    }
    public int getItemid() {
        return this.itemid;
    }
    public String getSubsel() {
        return this.subsel;
    }
    public String getuserName() {
        return this.username;
    }

   //no cardify happens in smart



}
