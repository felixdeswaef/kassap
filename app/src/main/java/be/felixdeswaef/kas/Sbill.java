package be.felixdeswaef.kas;

import java.util.ArrayList;
import java.util.List;

public class Sbill {

    public DataHandler dh;
    public Integer id;
    public bill bill;
    public List<Sbillit> items = new ArrayList<>();

    public Sbill(bill bi, DataHandler DH) {
        this.bill = bi;
        dh = DH;
        this.Smartify(DH);
    }

    void Smartify(DataHandler DH) {
        id = this.bill.getId();

    }

    void subscribe(Sbillit bit) {
        items.add(bit);

    }
    carddata scardify(){
       carddata card = this.bill.cardify();
       card.TLine = dh.FindPlace(bill.getplace()).place.getName();
       return card;
    }

}
