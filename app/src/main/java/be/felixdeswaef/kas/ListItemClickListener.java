package be.felixdeswaef.kas;

public interface ListItemClickListener {
    void onListItemClick(int clickedItemIndex);
}
