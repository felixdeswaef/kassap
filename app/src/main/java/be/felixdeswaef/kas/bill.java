package be.felixdeswaef.kas;

import com.google.gson.annotations.SerializedName;

//{{"id":1,"name":"Gerda Gerlach","placeid":21,"created_at":"2019-05-18 15:16:05","updated_at":"2019-05-18 15:16:05"}
public class bill {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("placeid")
    private Integer placeid;


    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public Integer getplace() {
        return this.placeid;
    }



    public carddata cardify()
    {
        return new carddata(this.name,"" ,"",this.placeid +"",this.id,"BIL");
    }
}
