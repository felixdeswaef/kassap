package be.felixdeswaef.kas;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.preference.PreferenceFragment;

public class SET_FRAG extends PreferenceFragmentCompat {
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.preferences, rootKey);
    }
}
